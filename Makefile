CC=gcc
CFLAGS=-std=c99 -Wall -Wextra -Wpedantic -O3 -DNOT_SUPERCOP -msse4.1 -flto
OBJS=$(patsubst %.c, %.o, $(wildcard *.c))

CXX=g++
CXXFLAGS=-std=c++11 -Wall -Wextra -Wpedantic -O3 -msse4.1 -fopenmp -flto
CXXOBJS=$(patsubst %.cpp, %.o, $(wildcard *.cpp))

OUT=rainbow

.PHONY: all clean default

default: $(OUT) $(CXXOBJS)
all: default

$(OUT): $(OBJS) $(CXXOBJS)
	$(CXX) $(OBJS) $(CXXOBJS) -fopenmp -flto -o $(OUT)
%.o: %.c
	$(CC) $(CFLAGS) -c $<
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $<

clean:
	-rm $(OBJS) $(CXXOBJS) $(OUT)
