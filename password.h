#pragma once

#include <cinttypes>
#include <array>
#include <memory>

#include <x86intrin.h>

extern "C" {
	int crypto_hash(unsigned char *out,
		const unsigned char *in,
		unsigned long long inlen);
}

namespace rainbow
{
	constexpr size_t password_len = 6;
	struct password
	{
		std::array<char, password_len> chars;

		bool operator==(const password& rhs) const {
			return chars == rhs.chars;
		}

		bool operator!=(const password& rhs) const {
			return chars != rhs.chars;
		}
	};

	constexpr size_t hash_size = 32;
	struct blake256_hash
	{
		alignas(16) std::array<unsigned char, hash_size> bytes;

		bool operator==(const blake256_hash& rhs) const {
			return bytes == rhs.bytes;
		}

		bool operator!=(const blake256_hash& rhs) const {
			return bytes != rhs.bytes;
		}
	};

	constexpr size_t num_ascii_chars = 64;
	const std::array<char, num_ascii_chars> ascii_mapping {
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
		'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',

		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',

		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',

		'@', '!', // Only those 2 symbols are allowed
	};

	inline blake256_hash hash(const password& pw)
	{
		blake256_hash result;
		crypto_hash(result.bytes.data(),
				reinterpret_cast<const unsigned char*>(pw.chars.data()),
				pw.chars.size());

		return result;
	}

	inline password reduce(const blake256_hash& hash, uint16_t k)
	{
		__m128i lhs = _mm_load_si128(
				reinterpret_cast<const __m128i*>(hash.bytes.data()));
		__m128i rhs = _mm_load_si128(
				reinterpret_cast<const __m128i*>(hash.bytes.data() + 16));

		__m128i xor16   = _mm_xor_si128(lhs, rhs);
		__m128i xored   = _mm_xor_si128(xor16, _mm_set1_epi16(k));
		__m128i result  = _mm_and_si128(xored, _mm_set1_epi8(num_ascii_chars - 1));

		alignas(16) std::array<unsigned char, 16> temp;
		_mm_store_si128(reinterpret_cast<__m128i*>(temp.data()), result);

		password reduced;
		for (size_t i = 0; i < reduced.chars.size(); i++){
			reduced.chars[i] = ascii_mapping[temp[i]];
		}
		return reduced;
	}
}
