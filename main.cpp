#define _FILE_OFFSET_BITS 64
#define _POSIX_C_SOURCE 200809L

#include "password.h"
#include "rainbow.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <algorithm>
#include <array>
#include <vector>
#include <utility>

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>

using namespace rainbow;
using namespace std;

struct entry
{
	password start;
	char space;
	password end;
	char newline;

	bool operator<(const password& pw) const {
		return end.chars < pw.chars;
	}

	entry()
	{ }

	entry(password end)
		: end(std::move(end))
	{ }
};

unsigned hex2bin(char input)
{
	if(input >= '0' && input <= '9'){
		return input - '0';
	}
	if(input >= 'A' && input <= 'F'){
		return input - 'A' + 10;
	}
	if(input >= 'a' && input <= 'f'){
		return input - 'a' + 10;
	}

	return 0;
}

blake256_hash from_hex(const char* str)
{
	blake256_hash hash;

	for (size_t i = 0; i < hash.bytes.size(); i++){
		hash.bytes[i] = (hex2bin(str[2 * i]) << 4) | hex2bin(str[2 * i + 1]);
	}

	return hash;
}

bool crack_password(const blake256_hash& initial_hash, unsigned k,
		const entry* entries, size_t n)
{
	password pw = reduce(initial_hash, k);
	entry dummy(pw);

	for (unsigned i = k + 1; i < 2048; i++){
		blake256_hash new_hash = rainbow::hash(pw);
		pw = reduce(new_hash, i);
	}

	const entry* start = entries;
	const entry* end   = entries + n;

	const entry* item = lower_bound(start, end, pw);
	if (item->end != pw){
		return false;
	}

	password found = item->start;
	for (unsigned i = 0; i < k; i++){
		blake256_hash new_hash = rainbow::hash(found);
		found = reduce(new_hash, i);
	}

	if (initial_hash == rainbow::hash(found)){
		printf("FOUND PASSWORD: %.6s\n", found.chars.data());
		return true;
	}

	// False alarm
	return false;
}

int main(int argc, const char** argv)
{
	if (argc == 1){
		std::vector<chain> results = create_rainbow_table();

		FILE* out = fopen("output-rainbow.txt", "w");
		for (const auto& e : results){
			const auto& start_array = e.chain_start.chars;
			const auto& end_array   = e.chain_end.chars;
			fprintf(out, "%.*s %.*s\n",
					static_cast<int>(start_array.size()), start_array.data(),
					static_cast<int>(end_array.size()),   end_array.data());
		}
		fclose(out);

		return EXIT_SUCCESS;
	}

	if (strlen(argv[1]) != 64){
		fputs("Need to pass 256-bit hash\n", stderr);
		return EXIT_FAILURE;
	}

	for (const char* str = argv[1]; *str != '0'; str++){
		if(*str >= '0' && *str <= '9'){
			continue;
		}
		if(*str >= 'A' && *str <= 'F'){
			continue;
		}
		if(*str >= 'a' && *str <= 'f'){
			continue;
		}

		fputs("Need to pass hex string\n", stderr);
		return EXIT_FAILURE;
	}
	blake256_hash hash = from_hex(argv[1]);

	int fd = open("sorted.txt", O_RDONLY);
	struct stat st;
	fstat(fd, &st);

	void* ptr = mmap(NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
	posix_madvise(ptr, st.st_size, POSIX_MADV_WILLNEED);
	const entry* contents = reinterpret_cast<const entry*>(ptr);
	size_t len = st.st_size / sizeof(entry);

	for (int i = 2048; i --> 0;){
		if (crack_password(hash, i, contents, len)){
			break;
		}
	}

	munmap(ptr, st.st_size);
	close(fd);
	return EXIT_SUCCESS;
}
