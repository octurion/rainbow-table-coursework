#include "rainbow.h"

#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>

using std::vector;
using std::fstream;

using namespace rainbow;

namespace {
	constexpr size_t table_entries = 67108864; // 64 million entries
	constexpr int chain_size       = 2048;
}

vector<chain> rainbow::create_rainbow_table()
{
	vector<chain> results(table_entries, chain());

	{
		fstream random("/dev/urandom", fstream::in);
		for (auto& e: results){
			random.read(e.chain_start.chars.data(), e.chain_start.chars.size());

			for (auto& b: e.chain_start.chars){
				b = ascii_mapping[b % ascii_mapping.size()];
			}

			e.chain_end = e.chain_start;
		}
		// Now /dev/urandom is closed here
	}

	
	printf("Now creating rainbow table\n"); 
	for (int step = 0; step < chain_size; step++){

		#pragma omp parallel for
		for (size_t i = 0; i < table_entries; i++){
			auto &e = results[i];
			blake256_hash hash_value = hash(e.chain_end);
			e.chain_end = reduce(std::move(hash_value), step);
		}

		printf("Step %d of chain completed\n", step);
	}

	return results;
}
