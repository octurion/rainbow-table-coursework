#pragma once

#include "password.h"

#include <utility>
#include <vector>

using std::vector;

namespace rainbow
{
	struct chain
	{
		password chain_start;
		password chain_end;

		chain(password chain_start, password chain_end)
			: chain_start(std::move(chain_start))
			, chain_end(std::move(chain_end))
		{ }

		chain(){ }
	};

	vector<chain> create_rainbow_table();
}
